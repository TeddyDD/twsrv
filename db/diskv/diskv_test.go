package diskv_test

import (
	"encoding/json"
	"os"
	"testing"
	"twsrv/db/diskv"
	"twsrv/storage"

	"github.com/stretchr/testify/require"
)

func TestStoryList(t *testing.T) {
	src := storage.NewTiddler()
	src.Type = diskv.NativeTiddletMime
	src.Title = "$:/StoryList"
	src.Meta["list"] = "foo bar"

	os.RemoveAll("testdata/storylist")
	db, err := diskv.New("testdata/storylist")
	require.NoError(t, err)

	err = db.Create(src)
	require.NoError(t, err)

	ret := storage.NewTiddler()
	err = db.Get(ret, src.Title)
	require.NoError(t, err)

	require.Equal(t, src, ret)
}

func TestKey(t *testing.T) {
	tests := map[string]struct {
		Title    string
		Type     string
		Expected string
	}{
		"simple native tiddler": {
			Title:    "hello world",
			Type:     diskv.NativeTiddletMime,
			Expected: "hello world.tid",
		},
		"native tiddler with .tid suffix": {
			Title:    "Hello World.tid",
			Type:     diskv.NativeTiddletMime,
			Expected: "Hello World.tid",
		},
		"json tiddler": {
			Title:    "hello.json",
			Type:     "application/json",
			Expected: "hello.json",
		},
		"some text tiddler": {
			Title:    "hello",
			Type:     "text/plain",
			Expected: "hello",
		},
		"story list": {
			Title:    "$:/StoryList",
			Type:     diskv.NativeTiddletMime,
			Expected: "$__StoryList.tid",
		},
	}

	db, err := diskv.New("testdata/keytest")
	require.NoError(t, err)

	for title, tc := range tests {
		t.Run(title, func(t *testing.T) {
			tid := storage.NewTiddler()
			tid.Title = tc.Title
			tid.Type = tc.Type
			require.Equal(t, tc.Expected, db.Key(tid))
		})
	}
}

func TestBasic(t *testing.T) {
	os.RemoveAll("testdata/simpledb")

	s := storage.NewTiddler()
	s.Title = "Hello World"
	s.Type = diskv.NativeTiddletMime
	s.Content = []byte(`some content
----
! yo
# bye
	`)
	meta := make(map[string]string)
	meta["tags"] = "foo bar baz"
	s.Meta = meta

	db, err := diskv.New("testdata/simpledb")
	require.NoError(t, err)

	err = db.Create(s)
	require.NoError(t, err)

	res := storage.NewTiddler()
	err = db.Get(res, "Hello World")
	lst, _ := db.List()
	require.NoError(t, err, "should retrieve Hello World", lst)

	require.Equal(t, s.Content, res.Content)
	require.EqualValues(t, s.Meta, res.Meta)
}

// Smoke test
func TestJsonTiddler(t *testing.T) {
	const title = "foo.json"
	tiddler := storage.NewTiddler()
	tiddler.Title = title
	tiddler.Type = "application/json"
	meta := make(map[string]string)
	meta["tags"] = "json foo bar"
	tiddler.Meta = meta

	db, err := diskv.New("testdata/persistent")
	require.NoError(t, err)

	t.Cleanup(func() {
		_ = db.Delete(title)
	})

	s := struct {
		Name string `json:"name"`
		Age  int    `json:"age"`
	}{
		Name: "Daniel",
		Age:  27,
	}
	content, err := json.MarshalIndent(&s, "", "\t")
	require.NoError(t, err)

	tiddler.Content = content

	// update where not exist
	err = db.Update(tiddler)
	require.Error(t, err)
	require.False(t, db.Has(title))

	// first create
	err = db.Create(tiddler)
	require.NoError(t, err)
	require.FileExists(t, "testdata/persistent/foo.json")

	require.True(t, db.Has(title))

	// correct update
	err = db.Update(tiddler)
	require.NoError(t, err)
	require.True(t, db.Has(title))

	// create when exists
	err = db.Create(tiddler)
	require.Error(t, err)

	list, err := db.List()

	require.NoError(t, err)
	require.Equal(t, []string{db.Key(tiddler)}, list)

	// retrieve back and compare
	res := storage.NewTiddler()
	err = db.Get(res, title)
	require.NoError(t, err)

	require.Equal(t, content, res.Content)
	require.Equal(t, meta, res.Meta)
	require.Equal(t, tiddler.Type, res.Type)
}

func TestNativeTiddler(t *testing.T) {
	os.RemoveAll("testdata/persistent")
	for _, title := range []string{"hello", "hello.tid"} {
		t.Run(title, func(t *testing.T) {
			tiddler := storage.NewTiddler()
			tiddler.Title = title
			tiddler.Type = diskv.NativeTiddletMime
			meta := make(map[string]string)
			meta["tags"] = "baz bar"
			tiddler.Meta = meta

			db, err := diskv.New("testdata/persistent")
			require.NoError(t, err)

			t.Cleanup(func() {
				_ = db.Delete(title)
			})

			tiddler.Content = []byte(`Hello world`)

			// update where not exist
			err = db.Update(tiddler)
			require.Error(t, err)
			require.False(t, db.Has(title))

			// first create
			err = db.Create(tiddler)
			require.NoError(t, err, "create should succeed")

			require.True(t, db.Has(title))

			require.FileExists(t, "testdata/persistent/hello.tid")

			// correct update
			err = db.Update(tiddler)
			lst, _ := db.List()
			require.NoError(t, err, "update should succeed", lst)
			require.True(t, db.Has(title))

			// create when exists
			err = db.Create(tiddler)
			lst, _ = db.List()
			require.Error(t, err, "creating same tiddler should fail", tiddler.Title, tiddler.Type, "generated key", db.Key(tiddler), lst)

			list, err := db.List()
			require.NoError(t, err)
			require.Equal(t, []string{db.Key(tiddler)}, list)

			// retrieve back and compare
			res := storage.NewTiddler()
			err = db.Get(res, title)
			require.NoError(t, err)

			require.Equal(t, tiddler.Content, res.Content)
			require.Equal(t, meta, res.Meta)
			require.Equal(t, tiddler.Type, res.Type)
		})
	}
}
