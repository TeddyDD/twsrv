package diskv

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"strings"
	"twsrv/storage"
	"twsrv/tiddlywiki"

	"github.com/peterbourgon/diskv/v3"
)

const NativeTiddletMime = "text/vnd.tiddlywiki"

const (
	NativeExtension = ".tid"
	MetaExtension   = ".meta"
)

var _ storage.Storage = &diskvStorage{}

type diskvStorage struct {
	path string
	db   *diskv.Diskv
}

func New(path string) (storage.Storage, error) {
	res := &diskvStorage{}
	res.db = diskv.New(diskv.Options{
		BasePath: path,
		AdvancedTransform: func(s string) *diskv.PathKey {
			return &diskv.PathKey{
				Path:     []string{},
				FileName: s,
			}
		},
		InverseTransform: func(pathKey *diskv.PathKey) string {
			return pathKey.FileName
		},
		CacheSizeMax: 1024 * 1024,
	})
	return res, nil
}

func (d diskvStorage) Key(tiddler *storage.Tiddler) string {
	title := tiddler.Title
	title = strings.TrimSuffix(title, NativeExtension)

	if tiddler.Type == NativeTiddletMime {
		return tiddlywiki.ToFilename(title) + NativeExtension
	}

	return tiddlywiki.ToFilename(title)
}

func (d diskvStorage) Has(title string) bool {
	_, err := d.getKeys(title)
	if err != nil {
		return false
	}
	return true
}

func (d diskvStorage) List() ([]string, error) {
	res := []string{}
	for k := range d.db.Keys(nil) {
		if !strings.HasSuffix(k, MetaExtension) {
			res = append(res, k)
		}
	}
	return res, nil
}

func (d diskvStorage) Create(t *storage.Tiddler) error {
	k := d.Key(t)

	if d.db.Has(k) {
		return fmt.Errorf("key: %s already exists", k)
	}
	return d.put(t)
}

func (d diskvStorage) Update(t *storage.Tiddler) error {
	keys, err := d.getKeys(t.Title)
	if err != nil {
		return err
	}

	if !d.db.Has(keys.content) {
		return fmt.Errorf("key: %s not exists", keys.content)
	}
	if keys.meta != "" && !d.db.Has(keys.meta) {
		return fmt.Errorf("meta key: %s not exists", keys.meta)
	}
	return d.put(t)
}

func (d diskvStorage) Delete(title string) error {
	keys, err := d.getKeys(title)
	if err != nil {
		return err
	}
	contErr := d.db.Erase(keys.content)
	var metaErr error
	if keys.meta != "" {
		metaErr = d.db.Erase(keys.meta)
	}

	if contErr != nil || metaErr != nil {
		return fmt.Errorf("erase error: tiddler: %s, errors: %v %v", title, contErr, metaErr)
	}
	return nil
}

type keys struct {
	meta    string
	content string
}

// getKeys from tiddler title
// lookup order
// - title+.tid / no meta  -> native tiddler
// - title / title.meta -> other file type
// - title / no meta -> native tiddler with wrong extension or other .tid compatible file
func (d diskvStorage) getKeys(title string) (keys, error) {
	// convert to key
	res := keys{}

	k := strings.TrimSuffix(title, NativeExtension)
	k = tiddlywiki.ToFilename(k)

	keyWithTidExt := k + NativeExtension
	hasKeyWithTidExt := d.db.Has(keyWithTidExt)

	hasKey := d.db.Has(k)

	keyWithMetaExt := k + MetaExtension
	hasMeta := d.db.Has(keyWithMetaExt)

	// some title (stored as .tid) + no meta
	if hasKeyWithTidExt && !hasMeta {
		res.content = keyWithTidExt
		return res, nil
	}

	// some title + title.meta
	if hasKey && hasMeta {
		res.content = k
		res.meta = keyWithMetaExt
		return res, nil
	}

	// tid like file without meta
	if hasKey && !hasMeta {
		res.content = k
		return res, nil
	}
	return res, fmt.Errorf("coudn't get key %s, key found: %v, key Meta found: %v", k, hasKey, hasMeta)
}

func (d diskvStorage) Get(dst *storage.Tiddler, title string) error {
	keys, err := d.getKeys(title)
	if err != nil {
		return err
	}

	if keys.meta != "" {
		rawMeta, err := d.db.Read(keys.meta)
		if err != nil {
			return err
		}

		b := bytes.NewReader(rawMeta)
		DecodeMeta(b, dst)
	} else {
		raw, err := d.db.Read(keys.content)
		if err != nil {
			return err
		}

		// get meta
		err = DecodeMeta(bytes.NewBuffer(raw), dst)
		if err != nil {
			return err
		}

		b := bufio.NewReader(bytes.NewBuffer(raw))
		// skip until "^$"
		for line, err := b.ReadString('\n'); err == nil && line != "\n"; line, err = b.ReadString('\n') {
		}

		rest := &bytes.Buffer{}
		io.Copy(rest, b)
		dst.Content = rest.Bytes()
		return nil
	}

	rawContent, err := d.db.Read(keys.content)
	if err != nil {
		return err
	}
	dst.Content = rawContent
	return nil
}

func (d diskvStorage) put(t *storage.Tiddler) error {
	k := d.Key(t)
	if t.Type == NativeTiddletMime {
		k = strings.TrimSuffix(k, NativeExtension)
		return d.db.Write(k+NativeExtension, EncodeNative(t))
	}

	err := d.db.Write(k, t.Content)
	if err != nil {
		return err
	}

	b := &bytes.Buffer{}
	EncodeMeta(b, t)
	err = d.db.Write(k+MetaExtension, b.Bytes())
	if err != nil {
		d.db.Erase(t.Title)
		return err
	}

	return nil
}

func EncodeMeta(dst io.Writer, t *storage.Tiddler) {
	b := &bytes.Buffer{}
	fmt.Fprintf(b, "title: %s\n", t.Title)
	fmt.Fprintf(b, "type: %s\n", t.Type)
	for k, v := range t.Meta {
		fmt.Fprintf(b, "%s: %s\n", k, v)
	}
	io.Copy(dst, b)
}

func DecodeMeta(src io.Reader, t *storage.Tiddler) error {
	s := bufio.NewScanner(src)
	meta := t.Meta

loop:
	for s.Scan() {
		switch line := s.Text(); line {
		case "":
			break loop
		default:
			slice := strings.SplitN(line, ":", 2)
			if len(slice) != 2 {
				return fmt.Errorf("wrong format of meta line: %s", line)
			}
			meta[slice[0]] = strings.TrimSpace(slice[1])
		}
	}
	t.Type = meta["type"]
	t.Title = meta["title"]
	delete(meta, "title")
	delete(meta, "type")
	t.Meta = meta
	return nil
}

func EncodeNative(t *storage.Tiddler) []byte {
	b := &bytes.Buffer{}
	EncodeMeta(b, t)
	b.WriteRune('\n')
	b.Write(t.Content)
	return b.Bytes()
}
