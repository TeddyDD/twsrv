package storage

type Storage interface {
	Create(*Tiddler) error
	Update(*Tiddler) error
	// Get fetches tiddler with givet title to provided Tiddler object
	Get(*Tiddler, string) error
	Has(string) bool
	Delete(string) error
	// List returns list of all tiddler keys (not titles)
	List() ([]string, error)
	// Key returns Storage key for given tiddler
	Key(*Tiddler) string
}
