package storage

import (
	"encoding/json"
)

// Tiddler represents Tiddler in storage
type Tiddler struct {
	Title   string `json:"title"`
	Type    string `json:"type"`
	Meta    map[string]string
	Content []byte `json:"text"`
}

type SingleTiddlerJSON struct {
	Created  string            `json:"created"`
	Fields   map[string]string `json:"fields"`
	Title    string            `json:"title"`
	Type     string            `json:"type"`
	Text     string            `json:"text"`
	Modified string            `json:"modified"`
	Revision int               `json:"revision"`
	Bag      string            `json:"bag"`
}

func contains(s *[]string, key string) bool {
	for _, e := range *s {
		if e == key {
			return true
		}
	}
	return false
}

func SingleTiddler(t *Tiddler, revision int) ([]byte, error) {
	res := make(map[string]interface{})

	res["title"] = t.Title
	res["type"] = t.Type
	res["bag"] = "default"
	res["revision"] = revision

	res["fields"] = make(map[string]string)
	fields := res["fields"].(map[string]string)
	for k, v := range t.Meta {
		fields[k] = v
	}
	res["text"] = t.Content

	return json.Marshal(res)
}

// ToWire converts tiddler to WebApi json format
func (t Tiddler) ToWire(revision int) ([]byte, error) {
	out := make(map[string]interface{})
	out["title"] = t.Title
	out["type"] = t.Type
	out["revision"] = revision
	out["text"] = string(t.Content)
	for k, v := range t.Meta {
		out[k] = v
	}
	return json.Marshal(out)
}

func NewTiddler() *Tiddler {
	return &Tiddler{
		Meta: make(map[string]string),
	}
}
