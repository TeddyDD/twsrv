package main

import (
	"bytes"
	"crypto/md5"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
	"twsrv/db/diskv"
	"twsrv/storage"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

const statusJson = `{"username":"","anonymous":true,"read_only":false,"space":{"recipe":"default"},"tiddlywiki_version":"5.1.22"}`

var (
	db, _       = diskv.New("/tmp/foo")
	etagCounter = make(map[string]int)
)

func getTiddlyWiki() ([]byte, string) {
	tw, err := ioutil.ReadFile("static/empty.html")
	if err != nil {
		panic(err)
	}
	sum := md5.New()
	twBuf := bytes.NewBuffer(tw)
	io.Copy(sum, twBuf)
	eTag := fmt.Sprintf("%x", sum.Sum(nil))
	return tw, eTag
}

func main() {
	e := echo.New()
	e.Server.WriteTimeout = time.Second * 10
	e.Server.ReadTimeout = time.Second * 10
	e.Server.IdleTimeout = time.Second * 30

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.Use(middleware.Gzip())

	e.Group("/files", middleware.StaticWithConfig(middleware.StaticConfig{
		Root:   "wiki/files",
		Browse: false,
	}))

	twSrc, eTag := getTiddlyWiki()

	e.GET("/", func(c echo.Context) error {
		c.Response().Header().Set("Etag", eTag)
		c.Response().Header().Set("Cache-Control", "max-age=2592000")

		if match := c.Request().Header.Get("If-None-Match"); match != "" {
			if strings.Contains(match, eTag) {
				c.Response().WriteHeader(http.StatusNotModified)
				return nil
			}
		}

		return c.HTMLBlob(http.StatusOK, twSrc)
	})

	e.GET("/recipes/default/tiddlers.json", allTiddlersHandler)
	e.GET("/recipes/default/tiddlers/:title", getTiddler)
	e.GET("/status", getStatus)

	e.PUT("/recipes/default/tiddlers/:title", putTiddler)

	// why efault???
	e.DELETE("/bags/default/tiddlers/:title", deleteTiddler)
	e.DELETE("/bags/efault/tiddlers/:title", deleteTiddler)

	e.Logger.Fatal(e.Start(":1323"))
}

func allTiddlersHandler(c echo.Context) error {
	c.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	c.Response().WriteHeader(http.StatusOK)

	c.Response().Write([]byte(`[`))

	list, _ := db.List()
	fmt.Println("LIST", list)
	for i, key := range list {
	tiddler := storage.NewTiddler()
		db.Get(tiddler, key)
		out, _ := tiddler.ToWire(etagCounter[key])

		c.Response().Write(out)
		if i < len(list)-1 {
			c.Response().Write([]byte(`,`))
		}
	}
	c.Response().Write([]byte(`]`))

	return nil
}

func getTiddler(c echo.Context) error {
	title := c.Param("title")
	title, err := url.QueryUnescape(title)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}
	tiddler := storage.NewTiddler()
	err = db.Get(tiddler, title)
	if err != nil {
		c.Logger().Error("Not found tiddler", "title", title)
		return c.NoContent(http.StatusNotFound)
	}

	out, err := storage.SingleTiddler(tiddler, etagCounter[title])
	if err != nil {
		return c.NoContent(http.StatusInternalServerError)
	}

	c.Blob(http.StatusOK, echo.MIMEApplicationJSON, out)

	return nil
}

func getStatus(c echo.Context) error {
	return c.Blob(http.StatusOK, echo.MIMEApplicationJSON, []byte(statusJson))
}

type JSONTiddler struct {
	Bag         string            `json:"bag"`
	Created     string            `json:"created"`
	Creator     string            `json:"creator"`
	Modified    string            `json:"modified"`
	Modifier    string            `json:"modifier"`
	Permissions string            `json:"permissions"`
	Recipe      string            `json:"recipe"`
	Revision    string            `json:"revision"`
	Tags        []string          `json:"tags"`
	Text        string            `json:"text"`
	Title       string            `json:"title"`
	Type        string            `json:"type"`
	Uri         string            `json:"uri"`
	Fields      map[string]string `json:"fields"`
}

func putTiddler(c echo.Context) error {
	title := c.Param("title")
	title, err := url.QueryUnescape(title)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}
	j := &JSONTiddler{}
	err = c.Bind(j)
	if err != nil {
		c.Logger().Error(err)
	}
	fmt.Println("OUT", title, j)
	t := storage.NewTiddler()
	t.Title = j.Title
	t.Type = j.Type
	t.Content = []byte(j.Text)
	t.Meta = j.Fields

	if !db.Has(title) {
		db.Create(t)
	} else {
		db.Update(t)
	}
	etagCounter[title] += 1
	c.Response().Header().Set(echo.HeaderContentType, echo.MIMETextPlain)
	c.Response().Header()["etag"] = []string{fmt.Sprintf("default/%s/%d:", title, etagCounter[title])}
	return c.NoContent(http.StatusNoContent)
}

func deleteTiddler(c echo.Context) error {
	title := c.Param("title")
	title, err := url.QueryUnescape(title)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}
	db.Delete(title)

	return c.NoContent(http.StatusNoContent)
}
