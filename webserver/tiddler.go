package webserver

type Tiddler struct {
	Revision int    `json:"revision"`
	Title    string `json:"title"`
	Created  string `json:"created"`
	Modified string `json:"modified"`
	Tags     string `json:"tags,omitempty"`
	Type     string `json:"type"`
	Color    string `json:"color,omitempty"`
	List     string `json:"list,omitempty"`
	Due      string `json:"due,omitempty"`
	Text     string `json:"text,omitempty"`

	Fields map[string]interface{} `json:"-"`
}

type AllTiddlers []Tiddler
