package tiddlywiki

import (
	"regexp"
	"strings"
)

var lineStart = regexp.MustCompile(`^[./]`)
var invalidChars = regexp.MustCompile(`[\\/:*?"<>|\[\]()^#%&!@:+={}'~,]|\0`)

// ToFilename converts tiddler title to safe file name
func ToFilename(title string) string {
	const Replace = "_"

	res := strings.TrimSpace(title)
	res = lineStart.ReplaceAllString(res, Replace)
	res = invalidChars.ReplaceAllString(res, Replace)

	return res
}
