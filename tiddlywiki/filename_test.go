package tiddlywiki

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestTitleToFilename(t *testing.T) {
	var tests = map[string]string{
		"Simple Title": "Simple Title",
		".hidden":      "_hidden",
		"/dir":         "_dir",
		"_underscore_": "_underscore_",
		"..":           "_.",

		`./hello world`:             `__hello world`,
		`$:/foo/bar/baz`:            `$__foo_bar_baz`,
		`one, two, three`:           `one_ two_ three`,
		`[this is awesome tiddler]`: `_this is awesome tiddler_`,
		`foo/bar/baz`:               `foo_bar_baz`,
		`simple case`:               `simple case`,

		`[\/:*?"<>|\[\]()^#%&!@:+={}'~,]`: `_______________________________`,
	}

	for src, exp := range tests {
		t.Run(src, func(t *testing.T) {
			require.Equal(t, exp, ToFilename(src))
		})
	}
}
