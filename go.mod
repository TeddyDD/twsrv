module twsrv

go 1.15

require (
	github.com/labstack/echo/v4 v4.1.17
	github.com/peterbourgon/diskv/v3 v3.0.0
	github.com/stretchr/testify v1.4.0
)
